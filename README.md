# 基于excalidraw的开源白板应用

<img src="https://basic.dooring.cn/saas/user/7e200e46-4976-44e4-8af9-7082871b6934/FmNaolYwjQe03fi-63dzBsl-kXpZ.png?imageView2/0/format/webp/q/75" />

## demo地址

[在线白板应用](https://board.dooring.vip)

## 启动

```bash
# 进入应用主目录
cd excalidraw-app

# 安装依赖
yarn

# 本地运行
yarn start
```

## 打包

```bash
# 在excalidraw-app目录执行
yarn build
```

会产生一个`build`目录, 可以直接把`build`目录内的文件部署到服务器上即可.

## 技术交流 & 反馈

微信: cxzk_168






